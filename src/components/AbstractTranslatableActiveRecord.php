<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 28/03/17
 * Time: 13:37
 */

namespace quoma\yii2\translatable\components;

use quoma\yii2\translatable\models\Language;
use quoma\yii2\translatable\models\Translation;
use yii\base\DynamicModel;
use yii\base\UnknownPropertyException;

/**
 * Clase abastracta a implementar por modelos que se quieran traducir.
 *  - Define la funcionalidad base para la inicializacion y carga de datos.
 *  - Debe ser extendida por el modelo que se quiere traducir.
 *  - Define una funcion abstracta a ser implementada getTranslatableAttributes, la cual debe retornar un array con los atributos traducibles.
 *  - Define una funcion t($attribute, $language) que retorna un atributo traducido
 *      en el idioma pasado como parametro.
 *
 * @property integer $currency_id
 * @property string $name
 * @property string $iso
 * @property double $rate
 * @property string $status
 * @property string $code
 *
 * @property Translation[] $translations
 */
abstract class AbstractTranslatableActiveRecord extends \quoma\core\db\ActiveRecord
{
    public $translations;

    /**
     * Inicializo y despues cargo los
     */
    public function init() {
        parent::init();
        // Traigo los atributos definidos como traducibles.
        $fields = $this->getTranslatableAttributes();

        // Busco e itero todos los idiomas.
        $languages = Language::find()->cache(15)->all();
        foreach ( $languages as $lang ) {
            // Itero en cada atributo y lenguaje para crear el item de traduccion.
            foreach($fields as $field){
                $trans = new Translation();
                $trans->text            = '';
                $trans->language_id     = $lang->language_id;
                $trans->attribute       = $field;
                $this->translations[]   = $trans;
            }
        }
    }

    /**
     * Retorna un listado de los atributos que se pueden trasladar.
     * @return array
     */
    public abstract function getTranslatableAttributes();

    /**
     * Retorno el valor del atributo en base al lenguaje.
     *
     * @param $attribute
     * @param null $lang
     * @return mixed|string|void
     */
    public function t($attribute, $lang = null)
    {
        $lang = $lang ? $lang :  \Yii::$app->language;

        if(array_search($attribute, $this->getTranslatableAttributes()) !== false) {
            if ( is_array($this->translations)) {
                $value = $this->$attribute;
                foreach ($this->translations as $translation) {
                    if ($translation->attribute == $attribute && $translation->language->code == $lang) {
                        $value = $translation->text;
                    }
                }
                return $value;
            }
            return "";
        }
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $parentResult = parent::load($data, $formName);
        $translateResult = false;
        
        // Cargo las traducciones
        if(isset($data['Translation'])) {
            foreach ($data['Translation'] as $language_id => $attributes) {
                foreach ($attributes as $attrib => $value) {
                    foreach ($this->translations as $trans) {
                        if($trans->language_id == $language_id && $trans->attribute == $attrib) {
                            $trans->text = $value['text'];
                            
                            /**
                             * Para soportar modelos con multiples idiomas sin que ninguno
                             * de estos idiomas sea el idioma de la app, agregamos la condición
                             * del or, de esta forma, el atributo siempre se inicializa
                             */
                            if($trans->language->code == \Yii::$app->language || empty($this->$attrib)) {
                                $this->$attrib = $value['text'];
                            }
                        }
                    }
                }
            }
            $translateResult = true;
        }
        
        return $parentResult || $translateResult;
    }
}