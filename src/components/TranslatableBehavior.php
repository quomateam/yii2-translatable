<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 30/03/17
 * Time: 10:17
 */

namespace quoma\yii2\translatable\components;


use quoma\yii2\translatable\models\Language;
use quoma\yii2\translatable\models\Translation;
use yii\base\Behavior;
use yii\base\DynamicModel;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Class TranslatableBehavior
 * Behavior encargado del manejo de las traducciones asociadas a un modelo. 
 *
 * @package quoma\yii2\translatable\components
 */
class TranslatableBehavior extends Behavior
{
    /**
     * Si se utiliza un modelo extendido en frontend o backend, se debe
     * colocar el modelo a utilizar para relacionar los datos. Por ejemplo,
     * si tenemos common/models/Article, backend/models/Article y
     * frontend/models/Article, deberemos colocar modelClass = "common/models/Article"
     * para que los mismos datos sean asociados sin importar desde que lugar
     * de la aplicación se estan efectuando las llamadas.
     * @var string 
     */
    public $modelClass;
    
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }



    /**
     * Busco la traduccion de los atributos del modelo y los seteo en el array de translation
     */
    public function afterFind()
    {
        if( $this->owner instanceof AbstractTranslatableActiveRecord) {
            
            //para modelos entre common, backend y frontend
            $class = $this->modelClass ? $this->modelClass : get_class($this->owner);
            
            $translations = Translation::findByClass($class, $this->owner->primaryKey)->all();
            if (!empty($translations)) {
                foreach ( $translations as $translation) {
                    foreach ($this->owner->translations as $key => $empty) {
                        if ($translation->attribute == $empty->attribute && $translation->language_id == $empty->language_id ) {
                            $this->owner->translations[$key] = $translation;
                        }
                    }

                }
            }
        }
    }

    /**
     * Execute after model saved
     * @param \yii\base\Event $event
     */
    public function afterSave($event)
    {
        if(is_array($event->sender->translations)) {
            
            //para modelos entre common, backend y frontend
            $class = $this->modelClass ? $this->modelClass : get_class($this->owner);
            
            foreach( $event->sender->translations as $translation) {
                $translation->model_id = $event->sender->primaryKey;
                $translation->class = $class;
                $translation->save();
            }
        }
    }

    public function afterDelete($event)
    {
        if(is_array($event->sender->translations)) {
            foreach( $event->sender->translations as $translation) {
                $translation->delete();
            }
        }
    }

}