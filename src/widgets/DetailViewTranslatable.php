<?php
/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 3/04/17
 * Time: 12:21
 */

namespace quoma\yii2\translatable\widgets;

use quoma\yii2\translatable\models\Language;
use Yii;
use yii\base\Arrayable;
use yii\i18n\Formatter;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\widgets\DetailView;

/**
 * Class DetailViewTranslatable
 * Vista para usar con modelos que que implementen AbstractTranslatableActiveRecord.
 * 
 * @package quoma\yii2\translatable\widgets
 */
class DetailViewTranslatable extends DetailView
{

    /**
     * Renders a single attribute.
     * @param array $attribute the specification of the attribute to be rendered.
     * @param int $index the zero-based index of the attribute in the [[attributes]] array
     * @return string the rendering result
     */
    protected function renderAttribute($attribute, $index)
    {
        if (is_string($this->template)) {
            $captionOptions = Html::renderTagAttributes(ArrayHelper::getValue($attribute, 'captionOptions', []));
            $contentOptions = Html::renderTagAttributes(ArrayHelper::getValue($attribute, 'contentOptions', []));

            $value = "";
            // El atributo es una traduccion
            if(isset($attribute['attribute']) && array_search($attribute['attribute'], $this->model->getTranslatableAttributes())!==false) {
                $contentOptions = " style='text-align: left;'";
                $value = '<ul class="i18n" style="padding-left: 0; margin-bottom: 0;">';
                foreach ($this->model->translations  as $trans) {
                    if($trans->attribute == $attribute['attribute']) {
                        $value .= '<li><strong>'.$trans->language->name . ':</strong> ' . $trans->text .'</li>';
                    }
                }
                $value .= '</ul>';

            } else {
                $value = $this->formatter->format($attribute['value'], $attribute['format']);
            }

            return strtr($this->template, [
                '{label}' => $attribute['label'],
                '{value}' => $value,
                '{captionOptions}' => $captionOptions,
                '{contentOptions}' =>  $contentOptions,
            ]);
        } else {
            return call_user_func($this->template, $attribute, $index, $this);
        }
    }

}