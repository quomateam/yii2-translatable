<?php

namespace quoma\yii2\translatable\widgets;

use quoma\yii2\translatable\models\Language;
use yii\base\Widget;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

/**
 * Class TranslatableWidget
 * Widget que agrega el text o textarea a un tab para atributos a traducir.
 * 
 * @package quoma\yii2\translatable\widgets
 */
class TranslatableWidget extends Widget
{
    const TRANSLATABLE_INPUT_TEXT = 'text';
    const TRANSLATABLE_INPUT_TEXTAREA = 'textarea';
    /**
     * @var array $languages
     */
    private $languages;
    /**
     * @var string $attribute
     */
    public $attribute;

    public $form;

    public $model;

    public $field_type = TranslatableWidget::TRANSLATABLE_INPUT_TEXT;

    public $field_attributes = [];

    public function run()
    {
        $items = [];
        $attribs = [];
        $i = 0;
        
        foreach ($this->model->translations as $key => $trans) {
            if ($trans->attribute == $this->attribute ) {
                
                $attr = $this->attribute;
                $value = $this->model->$attr;

                if (empty($trans->text) && !empty($value)) {
                    $trans->text = $value;
                }

                if($this->field_type == TranslatableWidget::TRANSLATABLE_INPUT_TEXT){
                    $items[] = [
                        'label' => $trans->language->name,
                        'content' => $this->form
                            ->field($trans, '[' . $trans->language_id . ']['.$trans->attribute .']text' )
                            ->textInput($this->field_attributes)
                            ->label($this->model->getAttributeLabel($this->attribute)),
                        'options' => [
                            'class' => $i == 0 ? 'fade in' : 'fade',
                            'data-lang' => $trans->language->code
                        ],
                        'linkOptions' => [
                            'data-lang' => $trans->language->code
                        ]
                    ];
                } else if($this->field_type == TranslatableWidget::TRANSLATABLE_INPUT_TEXTAREA){
                    $items[] = [
                        'label' => $trans->language->name,
                        'content' => $this->form
                            ->field($trans, '[' . $trans->language_id . ']['.$trans->attribute .']text' )
                            ->textarea($this->field_attributes)
                            ->label($this->model->getAttributeLabel($this->attribute)),
                        'options' => [
                            'class' => $i == 0 ? 'fade in' : 'fade',
                            'data-lang' => $trans->language->code
                        ],
                        'linkOptions' => [
                            'data-lang' => $trans->language->code
                        ]
                    ];
                }
                
                $i++;
            }

        }
        
        if(!empty($items)){
            $this->registerJs();
        }
        
        return Tabs::widget(['items'=>$items]);
    }
    
    /**
     * Registramos un js para cambiar todos los tabs al mismo tiempo
     */
    private function registerJs()
    {
        $this->view->registerJs('var I18n = new function()
        {
            this.init = function(){
                bindEvents();
            }

            function bindEvents(){
                $("a[data-lang]").on("click", function(e){
                    var lang = $(e.target).attr("data-lang");
                    $("a[data-lang=\'"+lang+"\']").not(e.target).tab("show");
                });
            }
        }', \yii\web\View::POS_END, 'i18n-tabs');
        
        $this->view->registerJs('I18n.init();', \yii\web\View::POS_READY, 'i18n-tabs-init');
    }
}