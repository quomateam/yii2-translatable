<?php

/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 28/03/17
 * Time: 11:57
 */
namespace quoma\yii2\translatable\models;

use quoma\yii2\translatable\TranslatableModule;

/**
 * This is the model class for table "language".
 *
 * @property integer $language_id
 * @property string $name
 * @property string $code
 *
 */
class Language extends \quoma\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id'], 'integer'],
            [['code', 'name'], 'string']
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language_id' => TranslatableModule::t( 'ID'),
            'code' => TranslatableModule::t( 'Code'),
            'name' => TranslatableModule::t( 'Name'),
        ];
    }
    
    public function getTranslations()
    {
        return $this->hasMany(Translation::className(), ['language_id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        if($this->getTranslations()->exists()){
            return false;
        }
        return true;
    }

    public static function getName($code)
    {
        return self::findOne(['code'=>$code])->name;
    }
}