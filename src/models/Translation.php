<?php

/**
 * Created by PhpStorm.
 * User: cgarcia
 * Date: 28/03/17
 * Time: 11:57
 */
namespace quoma\yii2\translatable\models;

use quoma\yii2\translatable\TranslatableModule;
use yii\db\Query;

/**
 * This is the model class for table "translation".
 *
 * @property integer $translation_id
 * @property integer $language_id
 * @property integer $model_id
 * @property string $class
 * @property string $attribute
 * @property string $text
 *
 */
class Translation extends \quoma\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $new_rules = [];
        if($this->class) {
            $obj = new  $this->class();
            $rules = $obj->rules();
            foreach($rules as $rule) {
                foreach ($rule[0] as $attribute) {
                    if($attribute == $this->attribute) {
                        $new_rules[] = array_merge([
                            0 => ['text']
                        ], array_slice($rule,1) );
                        break;
                    }
                }
            }
        }

        return array_merge($new_rules, [
            [['language_id', 'model_id'], 'integer'],
            [['class', 'text', 'attribute'], 'string']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'translation_id' => TranslatableModule::t( 'ID'),
            'language_id' => TranslatableModule::t( 'Language'),
            'model_id' => TranslatableModule::t( 'Model'),
            'class' => TranslatableModule::t( 'Class'),
            'text' => TranslatableModule::t( 'Text'),
            'attribute' => TranslatableModule::t( 'Attribute'),
        ];
    }

    /**
     * @inheritdoc
     * Strong relations: None.
     */
    public function getDeletable()
    {
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id'])->cache(1);
    }

    /**
     * Retorno todas traducciones de una clase
     * @param $class
     * @return Query
     */
    public static function findByClass($class, $model_id) {
        return self::find()
            ->where(['class'=> $class, 'model_id'=> $model_id])
            ->orderBy(['attribute'=>SORT_ASC])
            ->cache(1)
        ;
    }

}