<?php

use yii\db\Migration;

class m170411_110754_fix_code_length extends Migration
{

    public function up()
    {
        $this->alterColumn('language', 'code', $this->string(5));
    }

    public function down()
    {
        $this->alterColumn('language', 'code', $this->string(3));
    }

}
