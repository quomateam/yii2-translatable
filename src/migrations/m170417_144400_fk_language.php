<?php

use yii\db\Migration;

class m170417_144400_fk_language extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_translation_language', 'translation', 'language_id', 'language', 'language_id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_translation_language', 'translation');
    }

}
