<?php

use yii\db\Migration;

class m170411_100754_translatable_tables extends Migration
{
    public function up()
    {
        $this->execute("create table language(
            language_id INT not null PRIMARY KEY AUTO_INCREMENT,
            name varchar(50),
            code varchar(3)
        );");

        $this->execute("create table translation(
          translation_id INT not null PRIMARY KEY AUTO_INCREMENT,
          language_id INT NOT NULL,
          model_id int not null,
          class varchar(100) not null,
          attribute varchar(50) not null,
          text text ); "
        );
        $this->execute("CREATE INDEX ix_translation_model_class ON translation (model_id, class);");

    }

    public function down()
    {
        echo "m170411_100754_translatable_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
