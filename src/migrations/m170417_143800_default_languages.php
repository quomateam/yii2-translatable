<?php

use yii\db\Migration;

class m170417_143800_default_languages extends Migration
{
    public function up()
    {
        $this->execute("INSERT INTO `language` (`language_id`, `name`, `code`) VALUES
            (1, 'Español Argentina', 'es_AR'),
            (2, 'English USA', 'en_US');");
    }

    public function down()
    {
        echo "m170417_143800_default_languages cannot be reverted.\n";

        return false;
    }

}
