<?php

namespace quoma\yii2\translatable;

use quoma\core\menu\Menu;
use quoma\core\module\QuomaModule;
use Yii;

class TranslatableModule extends QuomaModule
{
    public $controllerNamespace = 'quoma\yii2\translatable\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    /**
     * @return Menu
     */
    public function getMenu(Menu $menu)
    {
        return [];
    }

    public function getDependencies()
    {
        return [];
    }
}