# Instalación

En archivo composer.json agregar:

```
    "require": {
        "quoma/yii2-translatable": "*",
    }
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/quomateam/yii2-translatable.git"
        }
    ]
```

Ejecutar migración:
```
    ./yii migrate --migrationPath=@vendor/quoma/yii2-translatable/src/migrations
```

Modificar el lenguaje por defecto a los disponibles en la tabla 'language'.
```
    language' => 'es_AR',
```

# Modulo de traducción

Este modulo permite el manejo de traducciones de modelos, se hace mediante una tabla adicional
en la que se guarda el atributo, idioma, y clase del modelo.

El funcionamiento se hace atravez de una clase abstracta a extender y un behavior,
por lo que es transparente el uso de los modelos y controladores, lo que se tiene
que tener en cuenta es el uso de dos widgets, tanto para la visualizacion en views como
para la carga de los datos.

Se debe dejar la definición original del atributo a traducir, este se usara por defecto
y se usaran las validaciones definidas en el mismo para validar cada una de las traducciones.

#### Modelos

- Language: Define los idiomas que se pueden utilizar.
- Translation: Define cada atributo traducido.

#### Componentes
- quoma\yii2\translatable\components\AbstractTranslatableActiveRecord
    - Define la funcionalidad base para la inicializacion y carga de datos.
    - Debe ser extendida por el modelo que se quiere traducir.
    - Define una funcion abstracta a ser implementada getTranslatableAttributes, la cual debe retornar un array con los atributos traducibles. 
    - Define una funcion t($attribute, $language) que retorna un atributo traducido
    en el idioma pasado como parametro.
    - Define un atributo translations, donde estan todas las traducciones relacionadas
    para ese registro.

- quoma\yii2\translatable\componentes\TranslatableBehavior
    - Define la funcionalidad extra para el manejor de los modelos Translation.
    - Debe ser usada por la clase modelo a traducir.

#### Widgets
-  quoma\yii2\translatable\widgets\DetailViewTranslatable
    - Extension de DetailView que agrega los atributos traducibles con la lista de los idiomas y su traduccion.
     
-  quoma\yii2\translatable\widgets\TranslatableWidget
    - Renderiza los inputs del atributo a traducir con tabs.
    - Define dos tipos de inputs:

    - En la creacion del widget agrega dos parametros:
        - field_type:
            - TranslatableWidget::TRANSLATABLE_INPUT_TEXT, agrega un input tipo text
            - TranslatableWidget::TRANSLATABLE_INPUT_TEXTAREA, agrega un textarea
        - field_attributes, atributos extras pasados en la creación del input.


#### Uso

- Modelo:

```
#!php
/**
 * This is the model class for table "currency".
 *
 * @property integer $currency_id
 * @property string $name
 * @property string $iso
 * @property double $rate
 * @property string $status
 * @property string $code
 *
 * @property Bill[] $bills
 */
class Currency extends AbstractTranslatableActiveRecord
{
    /**
     * Retorna un listado de los atributos que se pueden trasladar.
     * @return array
     */
    public function getTranslatableAttributes()
    {
        return [
            'name'
        ];
    }
  
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translatable' => [
                'class' => 'quoma\yii2\translatable\components\TranslatableBehavior',
            ],
        ];
    }
    

```


    
    
- Formulario

```
#!php
    <?php echo \quoma\yii2\translatable\widgets\TranslatableWidget::widget(['model'=>$model, 'form'=>$form, 'attribute'=>'name']) ?>
```



- Vista

```
#!php
    <?= \quoma\yii2\translatable\widgets\DetailViewTranslatable::widget([
        'model' => $model,
        'attributes' => [
            'currency_id',
            'name',
            'iso',
            'rate',
            [
                'attribute'=>'status',
                'value'=>Yii::t('app', ucfirst($model->status))
            ],
            'code',
        ],
    ]) ?>
```

- Vista en Front End
```
Usar:
<?php echo $model->t('name') ?>
en vez de 
<?php echo $model->name ?>
```
